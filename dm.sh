#!/bin/bash
DOCKER_MACHINE_NAME=Default

# Create the machine if it doesn't already exist.
STATUS=`docker-machine.exe status ${DOCKER_MACHINE_NAME}`
if [ $? != 0 ]; then
   echo Creating the docker machine...
   docker-machine.exe create --driver "virtualbox" --virtualbox-memory "4096" ${DOCKER_MACHINE_NAME}
fi

# Start the machine if it is not already running.
if [ `docker-machine.exe status ${DOCKER_MACHINE_NAME}` != Running ]; then
   docker-machine.exe start ${DOCKER_MACHINE_NAME}
fi

# Tell the docker command to use this docker-machine
eval "$(docker-machine.exe env ${DOCKER_MACHINE_NAME} --shell bash)"

# Redefine DOCKER_CERT_PATH
# This is a Windows Path and has to be converted to a WSL path.
export DOCKER_CERT_PATH=$(echo ${DOCKER_CERT_PATH} | sed -e 's/\\/\//g' | sed -e 's/^\([A-Z]\):/\/mnt\/\L\1/')

# Test if it works
echo "Testing if it works by running 'hello-world'..."
docker run --rm hello-world
