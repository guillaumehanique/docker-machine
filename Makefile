DOCKER_MACHINE_NAME="Default"

.PHONY: help
help:
	@echo "Please use any of the following targets:"
	@echo "    create        Creates the machine if it doesn't already exist."
	@echo "    remove        Removes the docker machine."
	@echo "                  This permanently destroys all containers and images"
	@echo "                  that are stored on it."
	@echo "    stop          Stops the docker machine."

.PHONY: create
create:
	@echo "The machine cannot be created by 'make'."
	@echo "Please execute '. ./dm.sh'."

.PHONY: remove
remove:
	docker-machine.exe rm ${DOCKER_MACHINE_NAME} -y

.PHONY: stop
stop:
	docker-machine.exe stop ${DOCKER_MACHINE_NAME}
