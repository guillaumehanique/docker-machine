Docker-Machine
===

This repository helps creating docker-machines.

Introduction
---

### Problem situation

I work with docker images quite extensively. I used to work in Fedora, but they decided to go and work with `podman` which is probably better but only implements a subset of the features provided by `docker` (there is no `docker-compose` equivalent for example). So when I got myself a new laptop with Windows 10 Home Edition which comes with the `Windows Subsystem for Linux (WSL)` I decided to continue working in Windows instead of Linux. Another reason is that some tools like [Git Extensions](https://github.com/gitextensions/gitextensions) (which is by far the most amazing GUI to `Git` that I ever did see) and [WinMerge](https://winmerge.org/) only work on Windows.

### Problem description

First I tried to install `docker` in WSL. Though this does provide the `docker` command, the `docker` daemon cannot run in WSL...

Some googling suggested to install `Docker Desktop` and set the `DOCKER_HOST` variable correctly, after which one can use `docker` in WSL. But as it turns out in depends on `Hyper-V` which is removed from Windows Home...

### This repo

Instead of `Docker Desktop` one can also create a `docker-machine`, which will create a Virtual Machine in `VirtualBox`. Now if you set the `DOCKER_HOST` environment variable correctly you can use `docker` in WSL and doing so will actually run the images in the VM.

Getting started
---

### Windows

Some software must be installed on Windows. I used [Chocolatey](https://chocolatey.org/) to install the following packages:

* virtualbox
* docker-machine

Then you also need WSL, obviously. I installed `Ubuntu`. There are plenty of websites explaining how to do it. This is one: [https://linuxhint.com/install_ubuntu_windows_10_wsl/](https://linuxhint.com/install_ubuntu_windows_10_wsl/).

### WSL

Once you have `Ubuntu` installed you an start it using the Windows Start Menu. It will give you a shell.

Install `docker`. You can follow the instructions from [here](https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly#install-docker-and-docker-compose-within-wsl).

Creating a docker-machine
---

### Makefile

I like `make` a lot: you can add simple targets that execute commands without having to remember or document huge command lines with lots of arguments. It also has its downsides. One of them is that `make` can never set environment variables on the shell from which it is called.

The `Makefile` has two targets:

1. `create`. This doesn't actually do anything else than tell you to execute the `dm` command.
2. `remove`. If you created a docker machine with the `dm` command, this target will remove it. Note that there is no way to undo this. All your docker containers will be lost, as will any docker images that you did not push to a registry.

### dm.sh

"dm" stands for "Docker Machine", in case you haven't noticed :-). `dm.sh` will use `docker-machine.exe` to create a docker machine if one doesn't already exist, start it if it's not running and then set the environment variable `DOCKER_HOST` so that the `docker` command executes against the docker machine.

Just like `make` other shell scripts also cannot modify environment variables in the shell in which they are called. Unlike `make` shell scripts can if you invoke them with the dot space syntax. So instead of

```bash
./dm.sh
```

execute

```bash
. ./dm.sh
```
